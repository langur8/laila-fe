export default function({ app, store }) {
  app.$axios.onRequest(() => {
    store.commit('startRequest')
  })
  app.$axios.onResponse(() => {
    store.commit('finishRequest')
  })
}
