import Vue from 'vue'

export default function({ store }) {
  Vue.mixin({
    data() {
      return {
        restError: {},
        isSubmitted: false
      }
    },
    methods: {
      handleError(e) {
        if (
          typeof e.response !== 'undefined' &&
          typeof e.response.data !== 'undefined' &&
          typeof e.response.data.error !== 'undefined' &&
          typeof e.response.data.error.code !== 'undefined' &&
          e.response.data.error.code === 404
        ) {
          this.restError = { message: 'request_exception' }
        } else if (
          typeof e.response !== 'undefined' &&
          typeof e.response.data !== 'undefined' &&
          typeof e.response.data.error !== 'undefined' &&
          typeof e.response.data.error.code !== 'undefined' &&
          typeof e.response.data.error.message !== 'undefined'
        ) {
          if (e.message === 'internal_error') {
            this.error(500, e.message)
          }
          this.restError = e.response.data.error
        }
      },
      getFieldState(field) {
        return typeof this.restError.validation !== 'undefined' &&
          typeof this.restError.validation[field] !== 'undefined'
          ? false
          : null
      },
      getFieldMessage(field) {
        return this.getFieldState(field) === false
          ? this.restError.validation[field]
          : null
      },
      async makeRequest(url, data, method = this.$axios.$get) {
        if (this.isSubmitted === true) {
          return
        }
        this.isSubmitted = true
        this.restError = {}
        this.$nuxt.$loading.start()
        try {
          return await method(url, data)
        } catch (e) {
          this.handleError(e)
          this.isSubmitted = false
          throw e
        } finally {
          this.isSubmitted = false
          if (store.state.activeRequests === 0) {
            this.$nuxt.$loading.finish()
          }
        }
      }
    }
  })
}
