import Vue from 'vue'

Vue.mixin({
  methods: {
    formatTime(date) {
      if (date === '' || date === null || typeof date === 'undefined') {
        return ''
      }
      const dt = new Date(date)
      return dt.toLocaleDateString('cs', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
      })
    },
    formatDate(date) {
      if (date === '' || date === null || typeof date === 'undefined') {
        return ''
      }
      const dt = new Date(date)
      return dt.toLocaleDateString('cs', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
      })
    },
    formatDateString(date) {
      return this.formatDate(Date.parse(date))
    },
    dateValidity(date1, date2) {
      if (date1 === date2) {
        return new Date(date1).toLocaleDateString('en', {
          day: '2-digit',
          month: '2-digit',
          year: 'numeric'
        })
      }
      return (
        new Date(date1).toLocaleDateString('en', {
          day: '2-digit',
          month: '2-digit',
          year: 'numeric'
        }) +
        ' - ' +
        new Date(date2).toLocaleDateString('en', {
          day: '2-digit',
          month: '2-digit',
          year: 'numeric'
        })
      )
    },
    price(price, currency) {
      const priceDec = price / 100
      return (
        priceDec.toFixed(priceDec === parseInt(priceDec, 10) ? 0 : 0) +
        ' ' +
        this.$i18n.t(currency)
      )
    },
    serializeForm(data) {
      const replacer = function(key, value) {
        if (this[key] instanceof Date) {
          return this[key].getTime()
        }

        return value
      }
      return JSON.stringify(data, replacer)
    },
    formatDistance(value) {
      const formatter = new Intl.NumberFormat('en', {
        style: 'decimal',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0
      })
      return formatter.format(value) + ' m'
    },
    formatDistanceKm(value) {
      const formatter = new Intl.NumberFormat('en', {
        style: 'decimal',
        minimumFractionDigits: 0,
        maximumFractionDigits: 2
      })
      return formatter.format(value / 1000) + ' km'
    },
    getJsonFromUrl(url) {
      const question = url.indexOf('?')
      let hash = url.indexOf('#')
      if (hash === -1 && question === -1) return {}
      if (hash === -1) hash = url.length
      const query =
        question === -1 || hash === question + 1
          ? url.substring(hash)
          : url.substring(question + 1, hash)
      const result = {}
      query.split('&').forEach(function(part) {
        if (!part) return
        part = part.split('+').join(' ') // replace every + with space, regexp-free version
        const eq = part.indexOf('=')
        let key = eq > -1 ? part.substr(0, eq) : part
        const val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : ''
        const from = key.indexOf('[')
        if (from === -1) result[decodeURIComponent(key)] = val
        else {
          const to = key.indexOf(']', from)
          const index = decodeURIComponent(key.substring(from + 1, to))
          key = decodeURIComponent(key.substring(0, from))
          if (!result[key]) result[key] = []
          if (!index) result[key].push(val)
          else result[key][index] = val
        }
      })
      return result
    }
  }
})
Vue.directive('focus', {
  inserted(el) {
    el.focus()
  }
})
