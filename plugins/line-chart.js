import Vue from 'vue'
import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default function() {
  Vue.component('my-line-chart', {
    extends: Line,
    mixins: [reactiveProp],
    props: {
      options: {
        type: Object,
        default: null
      }
    },
    mounted() {
      // this.chartData is created in the mixin.
      // If you want to pass options please create a local options object
      this.renderChart(this.chartData, this.options)
    }
  })
}
