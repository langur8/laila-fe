/* global TWEEN */
import Vue from 'vue'

export default function() {
  Vue.component('animated-integer', {
    props: {
      value: {
        type: Number,
        required: true
      }
    },
    data() {
      return {
        tweeningValue: 0
      }
    },
    watch: {
      value(newValue, oldValue) {
        this.tween(oldValue, newValue)
      }
    },
    mounted() {
      this.tween(0, this.value)
    },
    methods: {
      tween(startValue, endValue) {
        const vm = this
        function animate() {
          if (TWEEN.update()) {
            requestAnimationFrame(animate)
          }
        }

        new TWEEN.Tween({ tweeningValue: startValue })
          .to({ tweeningValue: endValue }, 500)
          .onUpdate(function() {
            vm.tweeningValue = this.tweeningValue.toFixed(0)
          })
          .start()

        animate()
      }
    },
    template: '<span>{{ tweeningValue }}</span>'
  })
}
