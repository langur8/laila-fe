export const state = () => ({
  activeRequests: 0
})

export const mutations = {
  startRequest(state) {
    state.activeRequests++
  },
  finishRequest(state) {
    state.activeRequests--
  }
}
