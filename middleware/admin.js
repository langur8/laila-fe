export default function({ redirect, app }) {
  try {
    if (app.$auth.loggedIn === true) {
      if (
        typeof app.$auth.user.roles === 'undefined' ||
        app.$auth.user.roles.count === 0 ||
        !app.$auth.user.roles.some((val) => {
          return (
            val === 'ROLE_ADMIN' ||
            val === 'ROLE_SUPER_ADMIN' ||
            val === 'ROLE_SALES'
          )
        })
      ) {
        app.$auth.logout()
        return redirect('/login')
      }
    }
  } catch (e) {
    // console.log(e)
  }
}
